package main

import "fmt"

func main() {
	//bufferred
	c := make(chan int)

	go enviar(c)
	go recibir(c)

	fmt.Println("finalizando")

}
func enviar(c chan<- int) {
	c <- 42
}
func recibir(c <-chan int) {
	fmt.Println(<-c)
}
