package main

import "fmt"

func main() {
	//unbufferred
	ca := make(chan int)

	go func() {
		ca <- 42
	}()

	fmt.Println(<-ca)
}
