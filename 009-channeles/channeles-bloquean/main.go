package main

import "fmt"

func main() {
	//unbufferred
	ca := make(chan int)

	ca <- 42
	fmt.Println(<-ca)
}
