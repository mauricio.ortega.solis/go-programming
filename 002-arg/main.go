package main

import (
	"fmt"
	"runtime"
	"sync"
)

func main() {
	fmt.Printf("Numero de cpus: %v\n", runtime.NumCPU())
	fmt.Printf("Numero de Goroutines: %v\n", runtime.NumGoroutine())

	var wg sync.WaitGroup
	wg.Add(2)

	go func() {
		fmt.Println("Hola esta es mi primera go rutina")
		wg.Done()
	}()
	go func() {
		fmt.Println("Hola esta es mi segunda go rutina")
		wg.Done()
	}()
	wg.Wait()
	fmt.Println("a punto de finalozae")
	fmt.Printf("Numero de cpus: %v\n", runtime.NumCPU())
	fmt.Printf("Numero de Goroutines: %v\n", runtime.NumGoroutine())

}
