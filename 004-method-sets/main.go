package main

import "fmt"

type persona struct {
	nombre string
}

func (p *persona) hablar() {
	fmt.Println(p.nombre)
}

type humano interface {
	hablar()
}

func diAlgo(h humano) {
	h.hablar()
}

func main() {
	p1 := persona{
		nombre: "Mauro",
	}

	diAlgo(&p1)
	//diAlgo(p1)
	p1.hablar() //aca como hay direccion lo puedo llamar
}
